package com.serg.android.git;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonTheme1;
    private Button buttonTheme2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        buttonTheme1 = findViewById(R.id.theme1);
        buttonTheme2 = findViewById(R.id.theme2);

        buttonTheme1.setOnClickListener(this);
        buttonTheme2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        if (v == buttonTheme1)
//            //getApplication().setTheme(R.style.AppTheme);
//        else if (v == buttonTheme2)
//            //getApplication().setTheme(R.style.AppTheme2);
    }
}
